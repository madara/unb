<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('faculty_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->integer('year_of_study')->nullable();
            $table->string('academic_year')->nullable();
            $table->integer('semester')->nullable();
            $table->string('file');
            $table->string('mime');
            $table->tinyInteger('approved')->default(1);
            $table->timestamps();
            $table->index('user_id');
            $table->index('faculty_id');
            $table->index('course_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('faculty_id')->references('id')->on('faculties')->onDelete('cascade');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetables');
    }
}
