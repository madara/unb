<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserfeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersFeeds', function (Blueprint $table) {
            //

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('to');
            $table->string('message');
            $table->string('read')->default(1);
            $table->timestamps();
           $table->index('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('usersFeeds');
    }
}
