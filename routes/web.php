<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEndController@index')->name('/');
Route::get('/events/show', 'FrontEndController@event')->name('front.events');
Route::get('/upcomingEvent/{id}', 'FrontEndController@showEvent')->name('showevent');
Route::get('/notices/show', 'FrontEndController@notices')->name('frontend.notices');
Route::get('/contact-us', 'FrontEndController@contact')->name('frontend.contact');
Route::get('/timetables/master', 'FrontEndController@master')->name('frontend.master');
Route::get('/timetables/course', 'FrontEndController@course')->name('frontend.course');
Route::get('/allnotices/{id}', 'FrontEndController@showNotice')->name('frontnotices.show');
Route::get('/noPermission', function(){
    return view('nopermission');
})->name('nopermission');
Auth::routes();

Route::get('notices/viewimage/{filename}',function ($filename){
    $folder='notices';
    $path = public_path().'\\notices\\'.$filename;

    if(!File::exists($path)) abort(404);
    $file = File::get($path);
    $type = File::mimeType($path);

    $entry=Notice::where('file', '=', $filename)->firstOrFail();

    //  return (new Response($file, 200))
    // ->header(['Content-Type'=> $entry->mime]);
    return Response::make($file, 200, [
        'Content-Type' => $entry->mime,
        'Content-Disposition' => 'inline; filename="'.$entry->file.'"'

    ]);
})->name('getentry');

Route::group(['prefix'=>'admin', 'middleware'=>['auth', 'roles'], 'roles'=>['admin']],function (){


    Route::get('/','AdminController@createUsers')->name('admin.index');

    Route::get('/users', 'AdminController@createUsers')->name('users.create');
    Route::post('/students/save', 'AdminController@saveStudent')->name('student.save');
    Route::get('/users/manage', 'AdminController@manageUsers')->name('users.manage');
    Route::post('/users/update', 'AdminController@updateUser')->name('users.update');
    Route::post('users/delete/{id}', 'AdminController@deleteUser')->name('user.delete');
    Route::post('/faculty/add', 'AdminController@addFaculty')->name('addFaculty');
    Route::get('/courses/get','AdminController@getCourses')->name('getCourses');
    Route::post('/courses/add', 'AdminController@addCourse')->name('addCourse');
    Route::post('/lecturers/save', 'AdminController@saveLecturer')->name('lecturer.save');
    Route::get('/notices/create', 'NoticesController@index')->name('notices.index');
    Route::post('/notices/save', 'NoticesController@saveNotice')->name('notices.save');
    Route::get('/notices/manage', 'NoticesController@manageNotice')->name('notices.manage');
    Route::post('/notices/update', 'NoticesController@update')->name('noticeUpdate');
    Route::get('/notices/fetch/{id}', 'NoticesController@fetchNotice')->name('getData');
    Route::post('/notices/destroy/{id}', 'NoticesController@destroy')->name('notices.destroy');
    Route::get('/events/create', 'EventsController@create')->name('events.create');
    Route::post('/events/save', 'EventsController@save')->name('events.save');
    Route::get('/events/manage', 'EventsController@manage')->name('events.manage');
    Route::post('/events/destroy/{id}', 'EventsController@destroy')->name('events.destroy');
    Route::get('/event/{id}', 'EventsController@fetchEvent');
    Route::post('/event/', 'EventsController@eventUpdate')->name('event.update');
    Route::get('/timetables/create', 'TimetablesController@create')->name('timetables.create');
    Route::post('/timetables/masters/create', 'TimetablesController@createMaster')->name('masters.save');
    Route::post('/timetables/create', 'TimetablesController@createTimetable')->name('createTimetable');
    Route::get('timetables/manage', 'TimetablesController@manage')->name('timetables.manage');
    Route::post('timetables/delete/{id}', 'TimetablesController@destroyMaster')->name('master.destroy');
    Route::post('timetables/delete/timetable/{id}', 'TimetablesController@destroyTimetable')->name('timetable.destroy');
    Route::get('/notices/manage/user_notices', 'AdminController@manageUserNotice')->name('usernotices.manage');
    Route::get('/notice/{id}','AdminController@fetchNotice');
    Route::post('/notice/approve', 'NoticesController@approoveNotice')->name('userNotices.approve');
    Route::get('/events/manage/user/events', 'AdminController@manageUserEvent')->name('systemEvents.manage');
    Route::post('/event/approve/','EventsController@updateEvent')->name('userEvents.updateEvent');
    Route::get('/event/{id}','EventsController@fetchEvent');

    //reports
    Route::get('/reports/students','ReportController@students')->name('admin.reports.students');
    Route::post('/reports/students/generate', 'ReportController@studentgenerate')->name('admin.reports.students.generate');
    Route::get('/reports/supervisors','ReportController@supervisors')->name('admin.reports.supervisors');
    Route::post('/reports/supervisors/all', 'ReportController@supervisorgenerate')->name('admin.reports.lecturers.generate');
    Route::get('/reports/notices','ReportController@notices')->name('admin.reports.notices');
    Route::post('/reports/notices/all', 'ReportController@noticesgenerate')->name('admin.reports.notices.generate');
    Route::get('/reports/events','ReportController@events')->name('admin.reports.events');
    Route::post('/reports/events/all', 'ReportController@eventsgenerate')->name('admin.reports.events.generate');


     Route::post('/password/change', 'AdminController@changePassword')->name('admin.changePassword');
    Route::get('/change/password', 'AdminController@password')->name('admin.password');

});
Route::group(['prefix'=>'student', 'middleware'=>['web','auth', 'roles'], 'roles'=>['student']], function(){
    Route::get('/', 'StudentController@index')->name('student.index');
    Route::get('/profile/updatepassword', 'StudentController@updatePassword')->name('updatePassword');
    Route::post('/profile/password/update', 'StudentController@firstLogin')->name('student.firstLogin');
    Route::get('/profile', 'StudentController@profile')->name('student.profile');
    Route::post('/update/basic', 'StudentController@updateBasic')->name('student.updateBasic');
    Route::post('/update/photo', 'StudentController@updatephoto')->name('student.updatePhoto');
    Route::post('/update/contact', 'StudentController@updateContact')->name('student.updateContact');
    Route::post('/password/change', 'StudentController@changePassword')->name('student.changePassword');
    Route::get('/update/password', 'StudentController@password')->name('student.password');
    Route::get('/notices/create', 'StudentNoticesController@create')->name('student.createNotice');
    Route::post('/notices/save', 'StudentNoticesController@saveNotice')->name('student.saveNotice');
    Route::get('/notices/manage', 'StudentNoticesController@manageNotice')->name('student.manageNotice');
    Route::get('/notices/fetch/{id}', 'StudentNoticesController@fetchNotice')->name('getData');
    Route::post('/notices/update', 'StudentNoticesController@update')->name('userNoticeUpdate');
    Route::post('/notices/destroy/{id}', 'StudentNoticesController@destroy')->name('userNotices.destroy');
    Route::get('/events/create', 'UserEventsController@create')->name('UserEvents.create');
    Route::post('/events/save', 'UserEventsController@save')->name('UserEvents.save');
    Route::get('/events/manage', 'UserEventsController@manage')->name('UserEvents.manage');
    Route::post('/events/destroy/{id}', 'UserEventsController@destroy')->name('userEvents.destroy');
    Route::get('/event/{id}', 'UserEventsController@fetchEvent');
    Route::post('/event/', 'UserEventsController@eventUpdate')->name('userEvents.update');
    Route::post('/events/register/{id}','UserEventsController@saveParticipant')->name('participant.save');
});
Route::group(['prefix'=>'lecturer', 'middleware'=>['web','auth', 'roles'], 'roles'=>['lecturer']],function(){

    Route::get('/', 'LecturerController@profile')->name('lecturer.index');

    Route::get('/profile/updatepassword', 'LecturerController@updatePassword')->name('lecturer.updatePassword');
    Route::post('/profile/password/update', 'LecturerController@firstLogin')->name('lecturer.firstLogin');
    Route::get('/profile', 'LecturerController@profile')->name('lecturer.profile');
    Route::post('/update/basic', 'LecturerController@updateBasic')->name('lecturer.updateBasic');
    Route::post('/update/profile', 'LecturerController@updatephoto')->name('lecturer.updatePhoto');
    Route::post('/update/contact', 'LecturerController@updateContact')->name('lecturer.updateContact');
    Route::post('/password/change', 'LecturerController@changePassword')->name('lecturer.changePassword');
    Route::get('/update/password', 'LecturerController@password')->name('lecturer.password');
    Route::get('/notices/create', 'LecturerNoticesController@create')->name('lecturer.createNotice');
    Route::post('/notices/save', 'LecturerNoticesController@saveNotice')->name('lecturer.saveNotice');
    Route::get('/notices/manage', 'LecturerNoticesController@manageNotice')->name('lecturer.manageNotice');
    Route::get('/notices/fetch/{id}', 'LecturerNoticesController@fetchNotice')->name('getData');
    Route::post('/notices/update', 'LecturerNoticesController@update')->name('lecturer.NoticeUpdate');
    Route::post('/notices/destroy/{id}', 'LecturerNoticesController@destroy')->name('lecturer.destroy');
    Route::get('/events/create', 'LecturerEventsController@create')->name('lecturerEvents.create');
    Route::post('/events/save', 'LecturerEventsController@save')->name('lecturerEvents.save');
    Route::get('/events/manage', 'LecturerEventsController@manage')->name('lecturerEvents.manage');
    Route::post('/events/destroy/{id}', 'LecturerEventsController@destroy')->name('lecturerEvents.destroy');
    Route::get('/event/{id}', 'LecturerEventsController@fetchEvent');
    Route::post('/event/', 'LecturerEventsController@eventUpdate')->name('lecturerEvents.update');
});
Route::get('/events/user/register/{id}', 'UserEventsController@showreg')->name('showreg');

##password resets links
Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm');
Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.getemail');
Route::post('/password/reset','ResetPasswordController@reset')->name('password.request');
Route::get('student/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('student.reset');
Route::get('admin/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.reset');
Route::get('lecturer/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('lecturer.reset');

