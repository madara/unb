<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Event extends Model
{
    //
    protected $fillable= ['name','description','venue','start_date','end_date','approved','reason'];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function getShortContentAttribute(){
        return substr($this->description,0,random_int(69,69)). '...';
    }
    public function getFrontShortContentAttribute(){
        return substr($this->description,0,random_int(280,350)). '...';
    }
    public function getIndexShortContentAttribute(){
        return substr($this->description,0,random_int(160,200)). '...';
    }
    public function users(){
        return $this->belongsToMany(User::class)->withTimestamps();
    }
    public function participants(){
        return $this->belongsToMany(Participant::class);
    }

    public function  scopeFilterEvents($query, $filters){
        if($month=$filters['month']){
            $month=Carbon::parse($month)->month;
            $query->whereMonth('created_at',$month);
            // dd($query->get());

        }
        if($year=$filters['year']){
            $year=Carbon::parse($year)->year;
            $query->whereYear('created_at', $year);
        }

    }
    public static function archives(){
        return static::selectRaw('year(created_at)as year,monthname(created_at) as month, count(*) AS published')
            ->orderByRaw('min(created_at)desc')
            ->groupBy('year', 'month')->get();
    }
}
