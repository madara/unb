<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Participant extends Model
{
    protected $fillable=['id', 'event_id', 'name', 'email', 'phone', 'id_no'];
    public $timetamps=true;
    public function event(){
        return $this->belongsTo(Event::class);
    }
    public function users(){
        return $this->hasMany(User::class);
    }
}
