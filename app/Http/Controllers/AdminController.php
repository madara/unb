<?php

namespace App\Http\Controllers;

use App\Course;
use App\Faculty;
use App\Notice;
use App\User;
use DB;
use Auth;
use App\Events\UserWasBanned;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Event;
use Hash;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware(['web', 'auth', 'roles']);
    }
    public function dashboard(){
        $user=DB::table('users')->find(5);
        $users=new User;
        event(new UserWasBanned($users));

        return view('admin.home');
    }
    public function createUsers(){
        $faculties=Faculty::all();
        return view('admin.users.create', compact('faculties'));
    }
    public function saveStudent(Request $request){
        $this->validate($request, ['admission_staff_no'=>'required|max:16|min:5|unique:users',
            'name'=>'required','email'=>'required|unique:users', 'phone'=>'required|unique:users']);
        User::create(['admission_staff_no'=>$request->admission_staff_no,'name'=>$request->name, 'email'=>$request->email,
            'id_number'=>$request->id_number, 'status'=>1, 'gender'=>$request->gender , 'role_id'=>$request->role_id,

            'faculty_id'=>$request->faculty, 'phone'=>$request->phone, 'course_id'=>$request->course, 'year'=>$request->year, 'password'=>bcrypt($request->admission_staff_no)]);


        //dd($request->all());
        $users=User::where('id', '!=', Auth::user()->id)->get();
        return redirect()->action('AdminController@manageUsers');

    }
    public function  saveLecturer(Request $request){
        //dd($request->all());
        $this->validate($request, ['admission_staff_no'=>'required|max:16|min:5|unique:users',
            'name'=>'required','email'=>'required|unique:users', 'phone'=>'required|unique:users']);
        User::create(['admission_staff_no'=>$request->admission_staff_no,'name'=>$request->name, 'email'=>$request->email,
            'id_number'=>$request->id_number, 'gender'=>$request->gender , 'role_id'=>$request->role_id,
            'faculty'=>$request->faculty, 'status'=>1, 'phone'=>$return->phone, 'password'=>bcrypt($request->admission_staff_no)]);
        $users=User::where('id', '!=', Auth::user()->id)->get();
        return redirect()->action('AdminController@manageUsers');
    }

    public function manageUsers(){
        if(Auth::check()){
            $users=User::where('id', '!=', Auth::user()->id)->get();
            //dd($users);
        }
        else{
            return redirect()->route('logout');
        }

        // dd($users);
        return view('admin.users.manage', compact('users'));
    }
    public function updateUser(Request $request){
        $user=User::findorfail($request->id);
        $user->update(['status'=>$request->status]);
        return back();

    }
    public function deleteUser($id){
        $user=User::find($id);
        $user->delete();
        return back();
    }
    public function addFaculty(Request $request){
       // dd($request->all());
        if($request->ajax()){

            return response(Faculty::create(['faculty'=>$request->faculty]));

        }

    }
    public function getCourses(Request $request){
        if($request->ajax()){
            //return response($request->all());
          return response(Course::where('faculty_id',$request->faculty_id)->get());
        }

    }
    public function  addCourse(Request $request){
        if($request->ajax()){
       return response(Course::create(['faculty_id'=>$request->faculty_id, 'course'=>$request->course]));
        }
    }
    public function manageUserNotice(){
        //$users_id=User::where([['admin',null],['role','student']])->pluck('id');
        $user=Auth::user();

        $status=Notice::all()->pluck('approved')->toArray();
        //dd($status);
        $notices =Notice::where('user_id','<>',$user->id)->get();

        $noticeCount =Notice::where('user_id','<>',$user->id)->count();
        //dd($noticeCount);
        $events =Event::where('user_id','<>',$user->id)->get();

        return view('admin.notices.manageUserNotices', compact(['notices','status','events', 'noticeCount','users_ids']));
    }
    public function fetchNotice($id)    {

        $request = new Request();
        $notice = Notice::findorfail($id);
        return $notice;
    }
    public function manageUserEvent(){
        $user=Auth::user();
        $events =Event::where('user_id','<>',$user->id)->get();
        return view('admin.events.manageUserEvents', compact('events'));
    }
    public function password(){

        return view('admin.password
            ');
    }

    public function changePassword(Request $request){
        //dd($request->all());
        //dd('here');
        // $user=Auth::user();
        //$user=findorfail($id);
        $user = User::find(Auth::id());
        //dd($user);
        $this->validate($request,[
            'old_password' => 'required',
            'password'=>'required|min:4',
            'password_confirmation'=>'required|same:password'
        ]);


        $hashedPassword=$user->password;
        //dd($hashedPassword);
        $password=$request->old_password;
        if (Hash::check($request->old_password, $hashedPassword)) {
            //Change the password

            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            //$user->passrec=0;
            $user->save();
        }
        else{
            return back()
                ->with('message','The specified password does not match the database password');
        }
        // Alert::success('Password Changed Successfuly', 'Changed!');
        Auth::login($user);
        return view('admin.home');
    }
}
