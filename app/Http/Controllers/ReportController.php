<?php

namespace App\Http\Controllers;
use App\Notice;
use App\Student;
use App\Event;
use App\User;
use Illuminate\Http\Request;
use PDF;

class ReportController extends Controller
{
    public function students(){
        $students = User::all();
        return view('admin.reports.students.index', compact('students'));
    }
    public function studentgenerate(Request $request){
        if($request->report=='all'){
            $students = User::all();
            $pdf=PDF::loadView('admin.reports.students.all',['students'=>$students]);
            return $pdf->download('all students.pdf');
        }
        if($request->report=='active'){
            $students = User::all();
            //dd($students);
            $pdf=PDF::loadView('admin.reports.students.active_students',['students'=>$students]);
            return $pdf->download('active students.pdf');
        }

        if($request->report=='inactive'){
            $students = User::all();
            $pdf=PDF::loadView('admin.reports.students.inactive_students',['students'=>$students]);
            return $pdf->download('inactive student accounts.pdf');
        }


    }
    public function supervisors(){
        return view('admin.reports.lecturers.index');
    }
    public function supervisorgenerate(Request $request){
        if($request->report=='all'){
            $lecturer = User::all();
            $pdf=PDF::loadView('admin.reports.lecturers.all',['lecturers'=>$lecturer]);
            return $pdf->download('all system lecturers.pdf');
        }
        if($request->report=='active'){
            $lecturers = User::all();
            $pdf=PDF::loadView('admin.reports.lecturers.active_lecturers',['lecturers'=>$lecturers]);
            return $pdf->download('active lecturer accounts.pdf');
        }
        if($request->report=='inactive'){
            $lecturers = User::all();
            $pdf=PDF::loadView('admin.reports.lecturers.inactive_lecturers',['lecturers'=>$lecturers]);
            return $pdf->download('inactive lecturer accounts.pdf');
        }


    }
    public function notices(){
        return view('admin.reports.notices.index');
    }
    public function noticesgenerate(Request $request){
        if($request->report=='all'){
           $notice=Notice::all();
            $pdf=PDF::loadView('admin.reports.notices.all',['notices'=>$notice]);
            return $pdf->download('all system notices.pdf');
        }
        if($request->report=='approved'){
            $notice=Notice::all();
            $pdf=PDF::loadView('admin.reports.notices.approved',['notices'=>$notice]);
            return $pdf->download('approved notices.pdf');
        }
        if($request->report=='pending'){
            $notice=Notice::all();
            $pdf=PDF::loadView('admin.reports.notices.pending',['notices'=>$notice]);
            return $pdf->download('pending notices.pdf');
        }
        if($request->report=='rejected'){
            $notice=Notice::all();
            $pdf=PDF::loadView('admin.reports.notices.denied',['notices'=>$notice]);
            return $pdf->download('denied notices.pdf');
        }


    }
    public function events(){
        return view('admin.reports.events.index');
    }
    public function eventsgenerate(Request $request){
        if($request->report=='all'){
            $event=Event::all();
            $pdf=PDF::loadView('admin.reports.events.all',['events'=>$event]);
            return $pdf->download('all system events.pdf');
        }
        if($request->report=='approved'){
            $event=Event::all();
            $pdf=PDF::loadView('admin.reports.events.approved',['events'=>$event]);
            return $pdf->download('approved events.pdf');
        }
        if($request->report=='pending'){
            $event=Event::all();
            $pdf=PDF::loadView('admin.reports.events.pending',['events'=>$event]);
            return $pdf->download('pending events.pdf');
        }
        if($request->report=='rejected'){
            $event=Event::all();
            $pdf=PDF::loadView('admin.reports.events.denied',['events'=>$event]);
            return $pdf->download('denied events.pdf');
        }


    }

}
