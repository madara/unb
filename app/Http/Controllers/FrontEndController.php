<?php

namespace App\Http\Controllers;

use App\Http\ViewComposers\Master;
use App\Timetable;
use Illuminate\Http\Request;
use App\Event;
use App\Notice;
use App\MasterTimetable;
use Carbon\Carbon;
class FrontEndController extends Controller
{
    public function index(){
        $events=Event::where('approved',1)->orderBy('id', 'desc')
            ->filterEvents(request(['month', 'year']))
            ->paginate(4);
        $notices=Notice::where('approved',1)->orderBy('id', 'desc')
            ->filter(request(['month', 'year']))
            ->paginate(4);
        return view('frontend.index', compact(['events', 'notices']));
    }
    public function event(){
        $events=Event::where('approved',1)->orderBy('id', 'desc')
            ->filterEvents(request(['month', 'year']))
            ->paginate(3);

        return view('frontend.events', compact('events'));
    }
    public function showEvent($id){
        $event=Event::findorfail($id);
        $events=Event::where('approved',1)->orderBy('id', 'desc')
            ->filterEvents(request(['month', 'year']))
            ->paginate(5);
        return view('frontend.showevent', compact(['event', 'events']));
    }

    public function notices(){
        $notices=Notice::where('approved',1)->orderBy('id', 'desc')
                        ->filter(request(['month', 'year']))
                        ->paginate(3);

        return view('frontend.notices',compact(['notices']));
    }
    public function showNotice($id){
        $notice=Notice::findorfail($id);
        $notices=Notice::where('approved',1)->orderBy('id', 'desc')
            ->filter(request(['month', 'year']))
            ->paginate(5);
        //dd($notice);
        return view('frontend.shownotice', compact(['notice','notices']));
    }
    public function contact(){
        return view('frontend.contact_us');
    }
    public function master(){
        $masters=MasterTimetable::paginate(5);
        return view('frontend.master_timetables', compact(['masters']));
    }
    public function course(){
        $timetables=Timetable::paginate(5);
        return view('frontend.course_timetables', compact(['timetables']));
    }
}
