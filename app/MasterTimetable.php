<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Faculty;
use App\Course;
class MasterTimetable extends Model
{
    protected $table='masters';
    protected $fillable=['user_id', 'faculty_id', 'file','mime', 'academic_year', 'semester'];
    public $timestamps=true;
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function faculty(){
        return $this->belongsTo(Faculty::class);
    }
    public function course(){
        return $this->belongsTo(Course::class);
    }
}
