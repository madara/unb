<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Faculty;
use App\Course;
class Timetable extends Model
{
    protected $fillable=['user_id', 'faculty_id', 'course_id', 'year_of_study', 'academic_year', 'semester', 'file', 'mime', 'approved'];
    public $timestamps=true;
    public  function user(){
        return $this->belongsTo(User::class);
    }
    public function faculty(){
        return $this->belongsTo(Faculty::class);
    }
    public function course(){
        return $this->belongsTo(Course::class);
    }
}
