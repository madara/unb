<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Notice extends Model
{
    //
    protected $fillable=['type','title','content','description','file','mime','approved','reason'];
    public function getShortContentAttribute(){

        return substr($this->content,0, random_int(401, 405)).'...';
    }
    public function getNoticeContentAttribute(){

        return substr($this->description,0, random_int(401, 405)).'...';
    }
    public function getFrontDescriptionAttribute(){

        return substr($this->description,0, random_int(150, 170)).'...';
    }
    public function getFrontContentAttribute(){
        return substr($this->content,0,random_int(150, 170)).'...';
    }
    public function getTableContentAttribute(){
        return substr($this->content,0,random_int(71, 71)).'...';
    }
    public function getDescriptionContentAttribute(){
        return substr($this->description,0,random_int(69,69)).'...';
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function scopeFilter($query, $filters){
        if($month=$filters['month']){
            $month=Carbon::parse($month)->month;
            $query->whereMonth('created_at',$month);
           // dd($query->get());

        }
        if($year=$filters['year']){
            $year=Carbon::parse($year)->year;
            $query->whereYear('created_at', $year);
        }

    }
    public static function archives(){

        return static::selectRaw('year(created_at)as year,monthname(created_at) as month, count(*) AS published')
            ->orderByRaw('min(created_at)desc')
            ->groupBy('year', 'month')->get();
    }
}
