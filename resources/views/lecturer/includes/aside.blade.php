            <aside id="sidebar" class="sidebar c-overflow">
                <div class="s-profile">
                    <a href="" data-ma-action="profile-menu-toggle">
                        <div class="sp-pic">
                            @if(Auth::user()->image!==null)
                            <img src="{{URL::to('/profile/'. Auth::user()->image)}}" alt="">
                             @else
                                @if(Auth::user()->gender=='Male')
                                    <img src="{{url('img/profile-pics/male.jpg')}}" alt="">
                                @elseif(Auth::user()->gender=='Female')
                                    <img src="{{url('img/profile-pics/male.jpg')}}" alt="">
                                @endif
                            @endif
                        </div>

                        <div class="sp-info">
                            @if(Auth::check())
                            {{Auth::user()->name}}
                            @else
                                header({{URL::to('logout')}})
                            @endif
                            <i class="zmdi zmdi-caret-down"></i>
                        </div>
                    </a>

                    <ul class="main-menu">

                        <li>
                            <a href="{{route('lecturer.profile')}}"><i class="zmdi zmdi-account"></i> View Profile</a>
                        </li>
                        <li>
                            <a href="{{route('lecturer.password')}}"><i class="zmdi zmdi-input-antenna"></i> Change Password</a>
                        </li>
                        <li>
                            <a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{csrf_field()}}
                            </form>
                        </li>
                    </ul>
                </div>

                <ul class="main-menu">
                    <li class="active">
                        <a href="{{route('lecturer.profile')}}"><i class="zmdi zmdi-home"></i> Home</a>

                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-notifications-active"></i> Notices</a>

                        <ul>
                            <li><a href="{{route('lecturer.createNotice')}}">Create</a></li>
                            <li><a href="{{route('lecturer.manageNotice')}}" >Manage</a> </li>
                        </ul>
                    </li>  
                    
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-calendar-note"></i> Events</a>
     
                        <ul>
                            <li><a href="{{route('lecturerEvents.create')}}">Create</a></li>
                            <li><a href="{{route('lecturerEvents.manage')}}">Manage</a></li>
                        </ul>
                        
                    </li>

                </ul>
            </aside>