@extends('lecturer.master')
@section('content')
   <div class="card">
    <div class="card-header">
        <h2>Create Notices</h2>
        <small>Create text or notice with file upload</small>
    </div>
       <div class="card-body card-padding">
           <div class="form-wizard-basic fw-container">
               <ul class="tab-nav" role="tablist">
                   <li  class="active" ><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Text Notice</a></li>
                   <li><a href="#tab2" data-toggle="tab">File Upload Notice</a></li>
               </ul>

               <div class="tab-content">
                   <div class="tab-pane active" role="tabpanel" id="tab1">
                       <br>

                       <div class="row">
                           <form method="POST" action="{{route('lecturer.saveNotice')}}" enctype="multipart/form-data">
                               {{csrf_field()}}
                               <input type="hidden" name="type" value="text_notice">
                               <div class="row">
                                   <div class="col-lg-6">
                                       <div class="input-group fg-float">
                                           <span class="input-group-addon"><i class="zmdi zmdi-text-format"></i></span>
                                           <div class="fg-line">
                                               <input type="text" class="form-control input-mask" name="title" required>
                                               <label class="fg-label f-14" >Title</label>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div class="row">
                                   <div class="col-lg-12 text_content">
                                       <br><br>
                                       <div class="input-group fg-float">
                                           <span class="input-group-addon"><i class="zmdi zmdi-format-align-justify"></i></span>
                                           <label>Notice Content</label>
                                           <div class="fg-line">
                                               <textarea class="form-control html-editor" name="content" id="sum"  required></textarea>

                                           </div>
                                       </div>

                                   </div>
                               </div>
                               <br>
                               <br>
                               <button type="submit" class="btn btn btn-primary m-b-15 m-l-25 p-r-20 ">Create</button>

                           </form>
                       </div>
                   </div>
                   <div class="tab-pane fade" id="tab2">
                       <br>
                       <div class="row">
                           <form method="POST" action="{{route('lecturer.saveNotice')}} "  enctype="multipart/form-data">
                               {{csrf_field()}}
                               <input type="hidden" name="type" value="file_upload">
                               <div class="row">
                                   <div class="col-lg-6">
                                       <div class="input-group fg-float">
                                           <span class="input-group-addon"><i class="zmdi zmdi-text-format"></i></span>
                                           <div class="fg-line">
                                               <input type="text" class="form-control input-mask" name="title" required>
                                               <label class="fg-label f-14" >Title</label>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div class="row">
                                   <div class="col-lg-7 file">
                                       <br><br>
                                       <div class="fileinput fileinput-new fg-float" data-provides="fileinput">
                                        <span class="btn btn-primary btn-xs btn-file m-l-25">
                                            <span class="fileinput-new">Click to Select file</span>
                                            <input type="file" name="file" id="file" required>
                                        </span>
                                           <span class="fileinput-filename"></span>
                                           <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                       </div>
                                   </div>
                               </div>
                               <div class="row">
                                   <div class="col-lg-12 description">
                                       <br><br>
                                       <div class="input-group fg-float">
                                           <span class="input-group-addon"><i class="zmdi zmdi-format-align-justify"></i></span>
                                           <label>Notice Content</label>
                                           <div class="fg-line">
                                               <textarea class="form-control html-editor" name="description" id="summernote" required ></textarea>

                                           </div>
                                       </div>

                                   </div>
                               </div>
                               <br>
                               <br>
                               <button type="submit" class="btn btn btn-primary m-b-15 m-l-25 p-r-20 ">Create</button>


                           </form>
                       </div>
                   </div>

                   <ul class="fw-footer pagination wizard">
                       <li class="previous first"><a class="a-prevent" href=""><i
                                       class="zmdi zmdi-more-horiz"></i></a></li>
                       <li class="previous"><a class="a-prevent" href=""><i
                                       class="zmdi zmdi-chevron-left"></i></a></li>
                       <li class="next"><a class="a-prevent" href=""><i
                                       class="zmdi zmdi-chevron-right"></i></a></li>
                       <li class="next last"><a class="a-prevent" href=""><i
                                       class="zmdi zmdi-more-horiz"></i></a></li>
                   </ul>
               </div>
           </div>
       </div>
   </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(window).on("load", function () {
            $('#file').val('');
        })

    </script>
@endsection