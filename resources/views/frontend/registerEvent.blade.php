@extends('frontend.index')
@section('section')
    <div id="section">
        <div class="container">
            <div class="block-header">
                <h2>Register to participate</h2>
            </div>
            <div class="card col-md-10">
                <div class="card-header">
                    <h2>{{$event->name}}
                        <small class=" m-b-5">Created by <em class="f-500 c-black m-b-5">{{$event->user->name}}</em> on <em class="f-500 c-black m-b-5">{{$event->created_at->toDayDateTimeString()}}</em></small>
                    </h2>

                </div>
                <div class="card-body card-padding">

                    <div class="row">
                        <form action="{{route('participant.save', $event->id)}}" method="POST" enctype="multipart/form-data" id="regform">
                            {{csrf_field()}}
                            @if(count($errors)>0)
                                <ul>
                                    @foreach($errors->all() as $error)

                                        <li class="alert alert-danger ">{{$error}}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="row">
                            <div class="form-group">
                                <label for="name" class="col-md-1 control-lable">Name</label>
                                <div class="col-md-5">
                                    <input type="text" name="name" class="form-control" value="{{Auth::user()->name}}"> <br>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="form-group">
                                <label for="name" class="col-md-1 control-lable">Email</label>
                                <div class="col-md-5">
                                    <input type="email" name="email" class="form-control" value="{{Auth::user()->email}}"> <br>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="form-group">
                                <label for="phone" class="col-md-1 control-lable">Phone</label>
                                <div class="col-md-5">
                                    <input type="phone" name="phone" class="form-control" value="{{Auth::user()->phone}}"> <br>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="form-group">
                                <label for="id" class="col-md-1 control-lable">ID</label>
                                <div class="col-md-5">
                                    <input type="id" name="id_no" class="form-control" placeholder="Id number" value="{{Auth::user()->id_number}}"> <br>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-success">Register</button> <br>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
@endsection