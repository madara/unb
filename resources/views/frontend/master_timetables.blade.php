@extends('frontend.master')
@section('content')
    <style type="text/css">
        .pagination{
        // border-radius: 0 !important;
            padding-left: 0 !important;
            margin: 18px 0 !important;
            display: -webkit-box !important;
            display: -webkit-flex!important;
            display: flex !important;
            border-radius: .25rem !important;

        }
        .pagination>li {
            display: inline-block !important;
            font-size: 2rem !important;
            border-radius: 2px;
            padding-left: 5px;
            padding-right: 5px;
        }
        .page-item{
            margin: 0 8px 0 0;
        }
        .readmore {
            color: #2d2d2d;
            font-size: 13px;
            text-decoration: underline;
            font-weight: 700;
        }
    </style>
    <div class="about_bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{url('/')}}"><img src="{{URL::to('images/responsive-logo.png')}}" class="responsive-logo img-fluid"
                                                alt="responsive-logo"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Master Timetables</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="notice">
        <div class="container">
            <div class="row">
                @foreach($masters as $master)
                    <div class="col-md-12">
                        <div class="event_date">
                            <div class="event-date-wrap">
                                <p>{{Carbon\Carbon::parse($master->created_at)->format('d')}}</p>
                                <span>{{Carbon\Carbon::parse($master->created_at)->format('M')}} .{{Carbon\Carbon::parse($master->created_at)->format('y')}}</span>
                            </div>
                        </div>
                        <div class="date-description">
                            <div class="blogtitle">
                                <h3>{{$master->faculty->faculty }} Master Timetable for academic year {{$master->academic_year }} semester {{$master->semester}}</h3>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <p>Created by: {{$master->user->name}}</p>
                                <i class="icon-speedometer fa fa-calendar-check-o" aria-hidden="true"></i>
                                <p >{{$master->created_at->toDayDateTimeString()}}</p>


                            </div>
                                <p>{{$master->file}}</p>
                            <a href="{{URL::to('timetables/'.$master->file)}}" target="_blank">
                                <button class="btn btn-success btn-xs"><i class="fa fa-eye"></i> View</button>
                            </a>
                            <hr class="event_line">
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="container" style="text-align: center!important; padding-left: 500px;">
                <p style="font-size: 3rem"> {{$masters->links()}}</p>
            </div>
        </div>

    </div>
@endsection

