@extends('frontend.master')
@section('content')
    
    <div class="login" style="background: #a2a2a2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div id="login-overlay" class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="well">
                                            <form id="loginForm" method="POST" action="{{route('login')}}"
                                                  novalidate="novalidate">
                                                {{csrf_field()}}
                                                <div class="form-group">
                                                    <label for="username" class="control-label">Username</label>
                                                    <input type="text" class="form-control {{$errors->has('admission_staff_no') ? ' has-error' : '' }}" id="username" name="admission_staff_no"
                                                           value="{{old('admission_staff_no')}}" required="" title="Please enter you username"
                                                           placeholder="username">
                                                    @if ($errors->has('admission_staff_no'))
                                                        <span class="help-block">
                                                            <strong style="color: #F44336">{{ $errors->first('admission_staff_no') }}</strong>
                                                         </span>
                                                    @endif
                                                    @if (Session::has('deactivated'))
                                                        <span class="help-block">
                                                            <strong style="color: #F44336">{{ Session::get('deactivated') }}</strong>
                                                         </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label for="password" class="control-label">Password</label>
                                                    <input type="password" class="form-control{{$errors->has('password') ? ' has-error' : '' }}" id="password"
                                                           name="password" value="" required=""
                                                           title="Please enter your password">
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                         </span>
                                                    @endif
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="remember"> Remember login
                                                    </label>
                                                </div>
                                                <button type="submit" class="btn btn-warning" id="js-subscribe-btn">LOG IN
                                                </button>
                                                <a href="{{url('/password/reset')}}" class="btn btn-link">
                                                    Forgot Your Password?
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

