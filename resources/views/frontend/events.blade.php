@extends('frontend.master')
@section('content')
    <style type="text/css">
        .pagination{
        // border-radius: 0 !important;
            padding-left: 0 !important;
            margin: 18px 0 !important;
            display: -webkit-box !important;
            display: -webkit-flex!important;
            display: flex !important;
            border-radius: .25rem !important;

        }
        .pagination>li {
            display: inline-block !important;
            font-size: 2rem !important;
            border-radius: 2px;
            padding-left: 5px;
            padding-right: 5px;
        }
        .page-item{
            margin: 0 8px 0 0;
        }
        .readmore {
            color: #2d2d2d;
            font-size: 13px;
            text-decoration: underline;
            font-weight: 700;
        }
    </style>
    <section class="events" style="background: #f9f2f4">

        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="event-title">Events</h2>
                </div>
            </div>
            <br>
            <div class="row">

                <div class="tab-content">
                    <div class="tab-pane active" id="upcoming-events" role="tabpanel">
                        @foreach($events as $event)
                             <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="event-date">
                                        <h4>{{Carbon\Carbon::parse($event->start_date)->format('d')}}</h4>
                                        <span>
                                            {{Carbon\Carbon::parse($event->start_date)->format('M')}} {{Carbon\Carbon::parse($event->start_date)->format('Y')}}
                                        </span>
                                    </div>
                                    <span class="event-time">{{Carbon\Carbon::parse($event->start_date)->format('h:i A')}}</span>
                                </div>
                                <div class="col-md-10">
                                    <div class="event-heading">
                                        <h3>{{$event->name}}</h3>

                                        <p><i class="fa fa-user" aria-hidden="true"></i> Created by: {{$event->user->name }}
                                            <i class="icon-speedometer fa fa-calendar-check-o" aria-hidden="true"></i> {{$event->created_at->toDayDateTimeString()}} </p>

                                        <p><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Start: {{Carbon\Carbon::parse($event->start_date)->toDayDateTimeString()}} </p>
                                        <p><i class="fa fa-calendar-check-o" aria-hidden="true"></i> End: {{Carbon\Carbon::parse($event->end_date)->toDayDateTimeString()}} </p>
                                        <p><i class="fa fa-location-arrow" aria-hidden="true"></i> Venue: {{$event->venue}} </p>
                                        <p>{!! $event->FrontShortContent !!}</p>
                                    </div>
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="event-toggle" data-parent="#accordion"
                                               href="{{route('showevent', $event->id)}}">Show Details</a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <hr class="event-underline">
                        </div>
                        @endforeach
                            <div class="container" style="text-align: center!important; padding-left: 500px;">
                                <p style="font-size: 3rem"> {{$events->links()}}</p>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

