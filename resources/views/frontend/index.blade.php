<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Online NoticeBoard System</title>

    <script src="../../cdn-cgi/apps/head/HXlSBfKFloaU_WEOQ2wj9snXuhc.js"></script>
    <link rel="stylesheet" href="{{URL::to('frontend/css/bootstrap.min.css')}}">

    <link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet">

    <link rel="stylesheet" href="{{URL::to('frontend/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{URL::to('frontend/css/simple-line-icons.css')}}">

    <link rel="stylesheet" href="{{URL::to('frontend/css/slick.css')}}">
    <link rel="stylesheet" href="{{URL::to('frontend/css/slick-theme.css')}}">
    <link rel="stylesheet" href="{{URL::to('frontend/css/owl.carousel.min.css')}}">
    <link href="{{URL::to('frontend/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

    <link href="{{URL::to('frontend/css/style.css')}}" rel="stylesheet">
</head>
<body>


@include('frontend.includes.header_top')
@include('frontend.includes.affix_menu')
@include('frontend.includes.slider')
@include('frontend.includes.welcome')
{{--@include('frontend.includes.courses')--}}
@include('frontend.includes.events')

<div class="detailed_chart">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom">
                <div class="chart-img">
                    <img src="images/chart-icon_1.png" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter">39</span> Teachers
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom chart_top">
                <div class="chart-img">
                    <img src="images/chart-icon_2.png" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter">2600</span> Students
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 chart_top">
                <div class="chart-img">
                    <img src="images/chart-icon_3.png" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter">56</span> Courses
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="chart-img">
                    <img src="images/chart-icon_4.png" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter">13</span> Years Exp.</p>
                </div>
            </div>
        </div>
    </div>
</div>

@include('frontend.includes.footer')


<script data-cfasync="false" src="../../cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js"></script>
<script src=" {{URL::to('frontend/js/jquery.min.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>
<script src=" {{URL::to('frontend/js/tether.min.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>
<script src="{{URL::to('frontend/js/bootstrap.min.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>

<script src="{{URL::to('frontend/js/slick.min.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>
<script src=" {{URL::to('frontend/js/waypoints.min.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>
<script src="{{URL::to('frontend/js/counterup.min.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>
<script src="{{URL::to('frontend/js/instafeed.min.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>
<script src="{{URL::to('frontend/js/owl.carousel.min.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>
<script src="{{URL::to('frontend/js/validate.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>
<script src="{{URL::to('frontend/js/tweetie.min.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>

<script src=" {{URL::to('frontend/js/subscribe.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>

<script src="{{URL::to('frontend/js/script.js')}} " type="ea6c261083a87e55b4fceb3f-"></script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/4f936b58/cloudflare-static/rocket-loader.min.js"
        data-cf-nonce="ea6c261083a87e55b4fceb3f-" defer=""></script>
</body>
</html>
