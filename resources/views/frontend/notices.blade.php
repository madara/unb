@extends('frontend.master')
@section('content')
    <style type="text/css">
        .pagination{
           // border-radius: 0 !important;
            padding-left: 0 !important;
            margin: 18px 0 !important;
            display: -webkit-box !important;
            display: -webkit-flex!important;
            display: flex !important;
            border-radius: .25rem !important;

        }
       .pagination>li {
            display: inline-block !important;
            font-size: 2rem !important;
           border-radius: 2px;
           padding-left: 5px;
            padding-right: 5px;
        }
        .page-item{
            margin: 0 8px 0 0;
        }
        .readmore {
            color: #2d2d2d;
            font-size: 13px;
            text-decoration: underline;
            font-weight: 700;
        }
    </style>
    <div class="notice" style="background: #f9f2f4">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="event-title">Notices</h2><br><br>
                </div>
            </div>

            <div class="row">
                @foreach($notices as $notice)
                <div class="col-md-12">
                    <div class="event_date">
                        <div class="event-date-wrap">
                            <p>{{Carbon\Carbon::parse($notice->created_at)->format('d')}}</p>
                            <span>{{Carbon\Carbon::parse($notice->created_at)->format('M')}} .{{Carbon\Carbon::parse($notice->created_at)->format('y')}}</span>
                        </div>
                    </div>
                    <div class="date-description">
                        <div class="blogtitle">
                            <h3>{{$notice->title}}</h3>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <p>Created by: {{$notice->user->name}}</p>
                            <i class="icon-speedometer fa fa-calendar-check-o" aria-hidden="true"></i>
                            <p >{{$notice->created_at->toDayDateTimeString()}}</p>


                        </div>
                        @if($notice->type=="text_notice")
                            <p>{!! $notice->ShortContent !!}</p>
                            <p ><a class="readmore" href="{{route('frontnotices.show', $notice->id)}}">Read More</a></p>
                        @elseif($notice->type=="file_upload")
                            <p>{!! $notice->NoticeContent !!}</p>
                            <p ><a class="readmore" href="{{route('frontnotices.show', $notice->id)}}">Read More</a></p>
                        @endif
                        <hr class="event_line">
                    </div>
                </div>
                @endforeach
            </div>

            <div class="container" style="text-align: center!important; padding-left: 500px;">
               <p style="font-size: 3rem"> {{$notices->links()}}</p>
            </div>
        </div>

    </div>
@endsection

