@extends('frontend.master')
@section('content')
    <style type="text/css">
        .pagination{
        // border-radius: 0 !important;
            padding-left: 0 !important;
            margin: 18px 0 !important;
            display: -webkit-box !important;
            display: -webkit-flex!important;
            display: flex !important;
            border-radius: .25rem !important;

        }
        .pagination>li {
            display: inline-block !important;
            font-size: 2rem !important;
            border-radius: 2px;
            padding-left: 5px;
            padding-right: 5px;
        }
        .page-item{
            margin: 0 8px 0 0;
        }
        .readmore {
            color: #2d2d2d;
            font-size: 13px;
            text-decoration: underline;
            font-weight: 700;
        }
    </style>
    <div class="about_bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{url('/')}}"><img src="{{URL::to('images/responsive-logo.png')}}" class="responsive-logo img-fluid"
                                                alt="responsive-logo"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Event</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="blog-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @if($event->file!==null)
                        @if($event->mime=='image/jpeg'||$event->mime=='image/png' || $event->mime=='image/jpg' )
                        <div class="blog-img_block">
                            <img src="{{URL::to('events/'.$event->file)}}" class="img-fluid" alt="blog-img">
                            <div class="blog-date">
                                <span>{{Carbon\Carbon::parse($event->created_at)->format('d-m-Y')}}</span>
                            </div>
                        </div>
                        @else
                            <div class="blog-img_block">
                                <div class="caption" style="padding-left: 200px">
                                    <a href="{{URL::to('events/'.$event->file)}}" target="_blank"><p>{{$notice->file}} <button class="btn btn-success btn-xs "><i class="fa fa-eye"></i>view</button></p></a>
                                </div>
                                <div class="blog-date">
                                    <span>{{Carbon\Carbon::parse($event->created_at)->format('d-m-Y')}}</span>
                                </div>
                            </div>
                         @endif
                    @else
                        <div class="blog-img_block">
                            <div class="blog-date">
                                <span>{{Carbon\Carbon::parse($event->created_at)->format('d-m-Y')}}</span>

                            </div>
                        </div>

                    @endif
                    <div class="blog-tiltle_block">
                        <h4>{{$event->name}}</h4>
                        <h6><a href=#"><i class="fa fa-user" aria-hidden="true"></i><span>{{$event->user->name}}</span> </a> |
                            <a href="#">
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                <span>{{$event->created_at->toDayDateTimeString()}}</span>
                            </a>
                        </h6>
                        <p><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Start: {{Carbon\Carbon::parse($event->start_date)->toDayDateTimeString()}} </p>
                        <p><i class="fa fa-calendar-check-o" aria-hidden="true"></i> End: {{Carbon\Carbon::parse($event->end_date)->toDayDateTimeString()}} </p>
                        <p><i class="fa fa-location-arrow" aria-hidden="true"></i> Venue: {{$event->venue}} </p>
                             {!!$event->description!!}
                    </div>
                    <div class="blog-tiltle_block">
                        <div class="blog-icons">
                            <div class="blog-share_block">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                    </li>
                                    <li>Share :</li>
                                </ul>
                            </div>
                        </div><!-- 

                        <ul class="nav nav-tabs blogpost-tab-wrap" role="tablist">
                            <li class="nav-item blogpost-nav-tab">
                                <a class="nav-link active" aria-controls="comments" data-toggle="tab" href="#comments" role="tab">Comments</a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                        <div class="blogpost-tabs">

                            <div class="tab-content">
                                <div class="tab-pane active" id="comments" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="blodpost-tab-img">
                                                        <img src="{{URL::to('images/admission-detail/admission_testi-img.jpg')}}" alt="#">
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="blogpost-tab-description">
                                                        <h6>Hanna Gover</h6>
                                                        <p>Hey, Great Article, i have read it so many times and felt in love
                                                            with it Sunt in culpa qui officia deserunt mollit anim id est
                                                            laborum</p>
                                                        <p class="blogpost-rply"><a href="blog-post.html#">Reply</a> <span>few hours ago</span>
                                                        </p>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="blodpost-tab-img">
                                                        <img src="{{URL::to('images/admission-detail/admission_testi-img.jpg')}}" alt="#">
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="blogpost-tab-description">
                                                        <h6>Hanna Gover</h6>
                                                        <p>Hey, Great Article, i have read it so many times and felt in love
                                                            with it Sunt in culpa qui officia deserunt mollit anim id est
                                                            laborum</p>
                                                        <p class="blogpost-rply"><a href="blog-post.html#">Reply</a> <span>March 28</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <form action="blog-post.html#">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Your Comments</label>
                                                            <textarea class="form-control" rows="6"> </textarea>
                                                        </div>

                                                    </div>
                                                    <div class="col-12">
                                                        <input type="submit" value="Submit Comment" class="btn btn-warning"/>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="blog-featured_post">
                        <h3>Featured Events</h3>
                        @foreach($events as $event)
                            <div class="blog-featured-img_block">
                                @if($event->mime=="image/png" || $event->mime=="image/jpg" ||$event->mime=="image/jpeg")
                                    <img src="{{URL::to('events/'.$event->file)}}" style="width: 137px" height="112px" class="img-fluid" alt="blog-featured-img">
                                @else

                                    <img src="{{URL::to('images/blog/blogpost-img_01.jpg')}}" class="img-fluid" alt="blog-featured-img">
                                @endif
                               <a href="{{route('showevent', $event->id)}}"> <h5>{{$event->name}}</h5></a>
                                    <p>{!!  $event->ShortContent!!}</p>
                            </div>
                            <hr>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

