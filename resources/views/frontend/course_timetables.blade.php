@extends('frontend.master')
@section('content')
    <style type="text/css">
        .pagination{
        // border-radius: 0 !important;
            padding-left: 0 !important;
            margin: 18px 0 !important;
            display: -webkit-box !important;
            display: -webkit-flex!important;
            display: flex !important;
            border-radius: .25rem !important;

        }
        .pagination>li {
            display: inline-block !important;
            font-size: 2rem !important;
            border-radius: 2px;
            padding-left: 5px;
            padding-right: 5px;
        }
        .page-item{
            margin: 0 8px 0 0;
        }
        .readmore {
            color: #2d2d2d;
            font-size: 13px;
            text-decoration: underline;
            font-weight: 700;
        }
    </style>
    <div class="about_bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{url('/')}}"><img src="{{URL::to('images/responsive-logo.png')}}" class="responsive-logo img-fluid"
                                                alt="responsive-logo"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Course Timetables</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="notice">
        <div class="container">
            <div class="row">
                @foreach($timetables as $timetable)
                    <div class="col-md-12">
                        <div class="event_date">
                            <div class="event-date-wrap">
                                <p>{{Carbon\Carbon::parse($timetable->created_at)->format('d')}}</p>
                                <span>{{Carbon\Carbon::parse($timetable->created_at)->format('M')}} .{{Carbon\Carbon::parse($timetable->created_at)->format('y')}}</span>
                            </div>
                        </div>
                        <div class="date-description">
                            <div class="blogtitle">
                                <h3>{{$timetable->faculty->faculty }}  Course Timetable for {{$timetable->course->course}} year {{$timetable->year_of_study }}  academic year {{$timetable->academic_year }} semester {{$timetable->semester}}</h3>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <p>Created by: {{$timetable->user->name}}</p>
                                <i class="icon-speedometer fa fa-calendar-check-o" aria-hidden="true"></i>
                                <p >{{$timetable->created_at->toDayDateTimeString()}}</p>


                            </div>
                            <p>{{$timetable->file}}</p>
                            <a href="{{URL::to('timetables/'.$timetable->file)}}" target="_blank">
                                <button class="btn btn-success btn-xs"><i class="fa fa-eye"></i> View</button>
                            </a>
                            <hr class="event_line">
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="container" style="text-align: center!important; padding-left: 500px;">
                <p style="font-size: 3rem"> {{$timetables->links()}}</p>
            </div>
        </div>

    </div>
@endsection

