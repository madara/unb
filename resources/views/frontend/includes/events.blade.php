<section class="event">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h2>Recent Notices</h2>
                <div class="event-img2">
                    @foreach($notices as $notice)
                    <div class="row mt-4">
                        @if($notice->type=='file_upload')
                          @if($notice->mime=='image/png'||$notice->mime=='image/jpeg'|| $notice->mime=='image/jpg')
                                <div class="col-sm-3">
                                    <img src="{{URL::to('/notices/'. $notice->file)}}" class="img-fluid"
                                         alt="event-img">
                                </div>
                           @endif
                        @endif
                            {{--@if($notice->type=='text_notice')--}}
                                {{--<img src="{{URL::to('images/upcoming-event-img.jpg')}}" class="img-fluid"--}}
                                     {{--alt="event-img">--}}
                            {{--@endif--}}
                        <div class="col-sm-9"><h3>{{$notice->title}} </h3>
                            <span>{{$notice->created_at->toDateString()}}</span>
                            @if($notice->type=='text_notice')
                                {!! $notice->FrontContent !!}
                            @elseif($notice->type=="file_upload")
                                {!! $notice->FrontDescription !!}
                            @endif
                            <a href="{{route('frontnotices.show', $notice->id)}}">Read More</a></div>
                    </div>

                     @endforeach
                </div>
            </div>
            <div class="col-lg-6">
                <h2>Upcoming Events</h2>
                <div class="row">
                    @foreach($events as $event)
                    <div class="col-md-12">
                        <div class="event_date">
                            <div class="event-date-wrap">
                                <p>{{Carbon\Carbon::parse($event->start_date)->format('d')}}</p>
                               <span>{{Carbon\Carbon::parse($event->start_date)->format('M')}} {{Carbon\Carbon::parse($event->start_date)->format('y')}}</span>
                            </div>
                        </div>
                        <div class="date-description">
                            <h3>{{$event->name}}</h3>
                            {!! $event->IndexShortContent !!}
                            <a href="{{route('showevent', $event->id)}}">Read More</a>
                            <hr class="event_line">
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>