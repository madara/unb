<div class="header-topbar" style="background:#253e6b">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-8 col-md-9">
                <div class="header-top_address">
                    <div class="header-top_list">
                        <span class="fa fa-phone"></span>+254703696050
                    </div>
                </div>
                <div class="header-top_login2">
                    <a href="{{url('/login')}}">Login </a>/
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <div class="header-top_login mr-sm-3">
                    <a href="{{url('/login')}}"><span class="fa fa-arrow-circle-right"></span>Login </a>
                </div>
            </div>
        </div>
    </div>
</div>