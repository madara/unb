<div col-md-3>
    <div class="row-offcanvas row-offcanvas-right" id="left">
        <div id="sidebar" class="sidebar-offcanvas">
            <br>
            <div class="col-md-12">
                <h3>Archives</h3>
                <ul class="nav nav-pills nav-stacked">

                    @foreach($archives as $archive)
                        <li class=" event" onclick="return changeClass()" ><a href="{{URL::to('upcomingEvent/?month='.$archive->month.'&year='.$archive->year)}}">{{ $archive->month.' '.$archive->year }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>