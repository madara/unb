<footer style="background-color:#253e6b">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="foot-logo">
                    <a href="index.html">
                        <img src="{{URL::to('images/logo.png')}}" width="140" height="144" class="img-fluid" alt="footer_logo">
                    </a>
                    <p>2018 © copyright
                        <br> All rights reserved.</p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="sitemap">
                    <h3>Navigation</h3>
                    <ul>
                        <li><a href="{{url('/notices/show')}}">Notices </a></li>
                        <li><a href="{{url('/events/show')}}">Events</a></li>
                        <li><a href="{{url('/contact-us')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="address">
                    <h3>Contact us</h3>
                    <p><span>Address: </span> Online Noticeboard 4000-1000, Nairobi, Kenya</p>
                    <ul class="footer-social-icons">
                        <li><a href=#"><i class="fa fa-facebook fa-fb" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin fa-in" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter fa-tw" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>