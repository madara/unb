<div class="row">
    <div class="col-md-12">
        <div class="subscribe">
            <h3>Newsletter</h3>
            <form id="subscribeform" action="https://demo.web3canvas.com/themeforest/unisco/php/subscribe.php"
                  method="post">
                <input class="signup_form" type="text" name="email" placeholder="Enter Your Email Address">
                <button type="submit" class="btn btn-warning" id="js-subscribe-btn">SUBSCRIBE</button>
                <div id="js-subscribe-result" data-success-msg="Success, Please check your email."
                     data-error-msg="Oops! Something went wrong"></div>

            </form>
        </div>
    </div>
</div>