<section class="clearfix about about-style2">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Welcome</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's
                    standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                    scrambled it to make a
                    type specimen book. It has survived not only five centuries</p>
                <img src="images/welcom_sign.png" class="img-fluid" alt="welcom-img">
            </div>
            <div class="col-md-4">
                <img src="images/campus/campus-img_05.jpg" class="img-fluid about-img" alt="#">
            </div>
        </div>
    </div>
</section>