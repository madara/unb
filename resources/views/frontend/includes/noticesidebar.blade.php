<div class="col-md-2">
    <div class="row-offcanvas row-offcanvas-right" id="left">
        <div id="sidebar" class="sidebar-offcanvas">
            <br><br><br><br><br>
            <div class="col-md-12">
                <h3>Archives</h3>
                <ul class="nav nav-pills nav-stacked">

                    @foreach($noticeArchives as $archive)
                        <li><a href="{{URL::to('allnotices/?month='.$archive->month.'&year='.$archive->year)}}">{{ $archive->month.' '.$archive->year }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>