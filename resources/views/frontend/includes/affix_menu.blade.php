<div data-toggle="affix">
    <div class="container nav-menu2">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar2 navbar-toggleable-md navbar-light bg-faded">
                    <button class="navbar-toggler navbar-toggler2 navbar-toggler-right" type="button"
                            data-toggle="collapse" data-target="#navbarNavDropdown">
                        <span class="icon-menu"></span>
                    </button>
                    <a href="{{url('/')}}" class="navbar-brand nav-brand2"><h2><b style="color: #253e6b">ONLINE NOTICEBOARD</b></h2></a>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                        <ul class="navbar-nav">
						    <li class="nav-item">

                                <a class="nav-link" href="{{url('/')}}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/notices/show')}}">Notices</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/events/show')}}">Events</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">
                                    Timetables<span class="glyphicon glyphicon-chevron-down pull-right"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="{{url('timetables/master')}}">Master</a></li>
                                    <li><a class="dropdown-item" href="{{url('timetables/course')}}">Course</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/contact-us')}}">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>