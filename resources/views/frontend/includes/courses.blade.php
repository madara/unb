<section class="our_courses">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Our Courses</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="courses_box mb-4">
                    <div class="course-img-wrap">
                        <img src="images/courses_1.jpg" class="img-fluid" alt="courses-img">
                        <div class="courses_box-img">
                            <div class="courses-link-wrap">
                                <a href="course-detail.html" class="course-link"><span>course Details </span></a>
                                <a href="admission-form.html" class="course-link"><span>Join today </span></a>
                            </div>

                        </div>
                    </div>

                    <div class="courses_icon">
                        <img src="images/plus-icon.png" class="img-fluid close-icon" alt="plus-icon">
                    </div>
                    <a href="course-detail.html" class="course-box-content">
                        <h3>Biochemistry</h3>
                        <p>When an unknown printer took a galley...</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="courses_box mb-4">
                    <div class="course-img-wrap">
                        <img src="images/courses_2.jpg" class="img-fluid" alt="courses-img">
                        <div class="courses_box-img">
                            <div class="courses-link-wrap">
                                <a href="course-detail.html" class="course-link"><span>course Details </span></a>
                                <a href="admission-form.html" class="course-link"><span>Join today </span></a>
                            </div>

                        </div>
                    </div>

                    <div class="courses_icon">
                        <img src="images/plus-icon.png" class="img-fluid close-icon" alt="plus-icon">
                    </div>
                    <a href="course-detail.html" class="course-box-content">
                        <h3>History</h3>
                        <p>When an unknown printer took a galley...</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="courses_box mb-4">
                    <div class="course-img-wrap">
                        <img src="images/courses_3.jpg" class="img-fluid" alt="courses-img">
                        <div class="courses_box-img">
                            <div class="courses-link-wrap">
                                <a href="course-detail.html" class="course-link"><span>course Details </span></a>
                                <a href="admission-form.html" class="course-link"><span>Join today </span></a>
                            </div>

                        </div>
                    </div>

                    <div class="courses_icon">
                        <img src="images/plus-icon.png" class="img-fluid close-icon" alt="plus-icon">
                    </div>
                    <a href="course-detail.html" class="course-box-content">
                        <h3>Human Sciences</h3>
                        <p>When an unknown printer took a galley...</p>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="courses_box mb-4">
                    <div class="course-img-wrap">
                        <img src="images/courses_4.jpg" class="img-fluid" alt="courses-img">
                        <div class="courses_box-img">
                            <div class="courses-link-wrap">
                                <a href="course-detail.html" class="course-link"><span>course Details </span></a>
                                <a href="admission-form.html" class="course-link"><span>Join today </span></a>
                            </div>

                        </div>
                    </div>

                    <div class="courses_icon">
                        <img src="images/plus-icon.png" class="img-fluid close-icon" alt="plus-icon">
                    </div>
                    <a href="course-detail.html" class="course-box-content">
                        <h3>Earth Sciences</h3>
                        <p>When an unknown printer took a galley...</p>
                    </a>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="index-2.html#" class="btn btn-default btn-courses">View all courses</a>
            </div>
        </div>
    </div>
</section>