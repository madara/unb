@extends('frontend.index')
@section('section')
@include('frontend.includes.noticesidebar')
    <div id="section col-md-9">
        <div class="container">
            @if($notice->type=='file_upload')
            <div class="card col-md-10">
                <div class="card-header">
                    <h2>{{$notice->title}}
                        <small class=" m-b-5">Created by <em class="f-500 c-black m-b-5">{{$notice->user->name}}</em> on <em class="f-500 c-black m-b-5">{{$notice->created_at->toDayDateTimeString()}}</em></small>
                    </h2>

                </div>
                <div class="card-body card-padding">
                    @if($notice->mime=='image/jpeg')
                    <div class="row">
                        <div class="col-md-12 col-md-10">
                            <div class="thumbnail">
                                <a  href="{{route('getentry', $notice->file)}}"> <img src="{{URL::to('notices/'.$notice->file)}}" alt="ALT NAME" id="img" class="img-responsive" /> </a>
                                <div class="caption" style="font-size: 9px;">
                                    <a href="{{route('getentry', $notice->file)}}"><p>{{$notice->file}}</p></a>
                                </div>
                                <div style="font-size: 20px;" >{!!$notice->description!!}</div>
                            </div>
                        </div>
                    </div>
                    @elseif($notice->mime=='image/png')
                        <div class="row">
                        <div class="col-md-12 col-md-12">
                        <div class="thumbnail" >
                            <a  href="{{route('getentry', $notice->file)}}"> <img src="{{URL::to('notices/'.$notice->file)}}" alt="ALT NAME" class="img-responsive" /> </a>
                            <div class="caption">
                                <a href="{{route('getentry', $notice->file)}}"><p>{{$notice->file}}</p></a>
                            </div>
                            <div style="font-size: 20px;">{!!$notice->description!!}</div>
                        </div>
                        </div>
                        </div>

                    @elseif($notice->mime=='application/pdf')
                        <div class="col-md-6">
                            <div class="thumbnail">
                                <a href="{{route('getentry', $notice->file)}}"> <iframe src="{{URL::to('notices/'.$notice->file)}}"  class="img-responsive" frameborder="1" style="width:100%;min-height:300px;"/> </iframe></a>
                                <div class="caption">
                                    <a href="{{route('getentry', $entry->file)}}"><p>{{$notice->file}}</p></a>
                                </div>
                            </div>
                            <div style="font-size: 20px;">{!!$notice->description!!}</div>
                        </div>
                    @endif
                        <small class="text-muted" style="font-size: 14px;">Posted On: {{$notice->created_at->toDayDateTimeString()}}</small>
                </div>
            </div>


            @else
                <div class="card col-md-10">
                    <div class="card-header">
                        <h2>{{$notice->title}}
                            <small class=" m-b-5">Created by <em class="f-500 c-black m-b-5">{{$notice->user->name}}</em> on <em class="f-500 c-black m-b-5">{{$notice->created_at->toDayDateTimeString()}}</em></small>
                        </h2>
                    </div>
                    <div class="card-body card-padding">
                      <div class="row">
                          <div class="col-md-12">
                            <div class="thumbnail">
                                <div class="caption">
                                   <div style="font-size: 20px;" ><p>{!!$notice->content!!} </p> </div>
                                    <small class="text-muted" style="font-size: 14px;">Posted On: {{$notice->created_at->toDayDateTimeString()}}</small>
                                </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            @endif
        </div>


    </div>
@endsection