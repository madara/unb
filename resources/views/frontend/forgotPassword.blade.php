@extends('frontend.master')
@section('content')
    <div class="login" style="background: #a2a2a2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div id="login-overlay" class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="well">
                                            <form id="loginForm"  method="POST" action="{{route('password.getemail')}}"
                                                  novalidate="novalidate">
                                                {{csrf_field()}}
                                                @if (session('status'))
                                                    <div class="alert alert-success">
                                                        {{ session('status') }}
                                                    </div>
                                                @endif
                                                <div class="form-group">
                                                    <label for="email" class="control-label">Email</label>
                                                    <input type="email" class="form-control {{$errors->has('email') ? ' has-error' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email Address">
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong style="color: #F44336">{{ $errors->first('email') }}</strong>
                                                         </span>
                                                    @endif
                                                    @if (Session::has('deactivated'))
                                                        <span class="help-block">
                                                            <strong style="color: #F44336">{{ Session::get('deactivated') }}</strong>
                                                         </span>
                                                    @endif
                                                </div>

                                                <button type="submit" class="btn btn-warning" id="js-subscribe-btn">Send Reset Link
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

