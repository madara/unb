<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Online NoticeBoard System</title>
    <!-- Vendor CSS -->

    <link href="{{URL::to('vendors/bower_components/fullcalendar/dist/fullcalendar.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <!-- CSS -->
    <link href="{{URL::to('vendors/bootgrid/jquery.bootgrid.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/events.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/app_1.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/app_2.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/summernote/dist/summernote.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css')}}" rel="stylesheet">
    {{--<link href="{{URL::to('css/jquery.dataTables.min.css')}}" rel="stylesheet">--}}
    <script src="{{URL::to('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <link href="{{URL::to('summernote/dist/summernote.css')}}" rel="stylesheet">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
@if(Auth::check()&& Auth::user()->passrec==0)
    @if(Auth::user()->roles=='student')
        <?php
        echo "<div class='col-md-6 col-md-offset-3 alert alert-danger'>";
        echo '<strong>Warning!</strong> Change Password Before Proceeding to This Page.';
        echo "<div>";
        header("refresh:4;url=http://localhost/unb/public/student/profile/updatepassword")
        ?>
    @else
        <?php
        echo "<div class='col-md-6 col-md-offset-3 alert alert-danger'>";
        echo '<strong>Warning!</strong> Change Password Before Proceeding to This Page.';
        echo "<div>";
        header("refresh:4;url=http://localhost/unb/public/lecturer/profile/updatepassword")
        ?>
    @endif
@else
    <body>
    @include('sweet::alert')

    <div id="app ">
        <nav class="navbar navbar-inverse navbar-fixed-top f-20">
            <div class="container">
                <div class="navbar-header m-l-30">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand m-l-30 f-20 f-700" href="{{ url('/') }}">
                        Online NoticeBoard System
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class=" nav navbar-nav">
                        <li><a href="{{route('front.events')}}">Upcoming Events</a></li>
                    </ul>
                    <!--
                                         <ul class=" nav navbar-nav">
                                            <li><a href="#">Updates</a></li>
                                         </ul> -->
                    <ul class=" nav navbar-nav">
                        <li><a href="{{route('frontend.notices')}}">All Notices</a></li>
                    </ul>
                    <!-- -->
                    <ul class=" nav navbar-nav">
                        <li><a href="#">TimeTables</a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->

                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <ul class="dropdown-menu" role="menu">
                                    @if(Auth::user()->roles->name=='admin')
                                        <li>
                                            <a href="{{route('admin.index')}}">
                                                Dashboard
                                            </a>
                                        </li>
                                    @elseif(Auth::user()->roles->name=='student')
                                        <li>
                                            <a href="{{route('student.index')}}">
                                                Dashboard
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{route('lecturer.index')}}">
                                                Dashboard
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{csrf_field()}}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif

                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>

    @yield('scripts')
    <!-- Javascript Libraries -->
    <script src="{{URL::to('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script src="{{URL::to('vendors/bower_components/flot/jquery.flot.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/flot.curvedlines/curvedLines.js')}}"></script>
    <script src="{{URL::to('vendors/sparklines/jquery.sparkline.min.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>

    <script src="{{URL::to('vendors/bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/fullcalendar/dist/fullcalendar.min.js')}} "></script>
    <script src="{{URL::to('vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/Waves/dist/waves.min.js')}}"></script>
    <script src="{{URL::to('vendors/bootstrap-growl/bootstrap-growl.min.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script src="{{URL::to('vendors/bootgrid/jquery.bootgrid.updated.min.js')}}"></script>
    <script src="{{URL::to('js/app.min.js')}}"></script>
    <script src="{{URL::to('js/custom.js')}}"></script>
    <script src="{{URL::to('vendors/input-mask/input-mask.min.js')}} "></script>
    <script src="{{URL::to('vendors/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{URL::to('vendors/summernote/dist/summernote-updated.min.js')}}"></script>
    <script src="{{URL::to('vendors/summernote/dist/summernote-updated.min.js')}}"></script>
    <script src="{{URL::to('vendors/fileinput/fileinput.min.js')}}"></script>
    <script src="{{URL::to('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{URL::to('summernote/dist/summernote.js')}}"></script>
    <script src="{{URL::to('js/custom.js')}}"></script>


    </body>
@endif
</html>