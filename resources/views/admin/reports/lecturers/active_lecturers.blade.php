<!DOCTYPE html>
<html>
<head>
    <title>Supervisors Reports</title>

    <link href="{{ asset('bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    {{--<link rel="stylesheet" href="{{asset('dataTables.bootstrap.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('public/plugins/dataTables/responsive.bootstrap.min.css')}}">
    <style type="text/css">
        hr.style2 {
            border-top: 3px double #8c8b8b !important;
            height: 1px;
        }
    </style>
</head>
<body>

<div style="text-align: center"><img style="text-align: center" width="125" height="125" src="{{URL::to('logo.png')}}" class="img-thumbnail"></div>
<figcaption><h3 style="text-align: center">Online NoticeBoard System</h3></figcaption>
<hr class="style2" style="font-size: larger;">
<h2 style="text-align: center">Active Lecturers</h2>
<p style="text-align: right">
    <?php
    date_default_timezone_set('Africa/Nairobi');
    echo " <b style='text-align:right!important;'> Printed On</b> : " . date("Y/m/d") . "<b> At</b> " . date("h:i:sa");
    ?>
</p>
<table class="table table-bordered table-hover toggle-circle default footable-loaded footable">
    <thead>
    <tr>
        <th># </th>
        <th>Name</th>
        <th width="80">Staff Number</th>
        <th>Faculty</th>
        <th>Email</th>
        <th width="80">Created on</th>

    </tr>
    </thead>
    <tbody>
    @foreach($lecturers as $key=>$lecturer)
        @if($lecturer->roles->name=='lecturer')
                @if($lecturer->passrec==1)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$lecturer->name}}</td>
                    <td>{{$lecturer->admission_staff_no}}</td>
                    <td>{{$lecturer->faculty->faculty}}
                    <td>{{$lecturer->email}}</td>
                    <td>{{Carbon\Carbon::parse($lecturer->created_at)->format('d-m-Y')}}</td>
                </tr>
                @endif
        @endif
    @endforeach
    </tbody>


</table>
<!-- jQuery -->
<script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('bootstrap/dist/js/bootstrap.min.js') }}"></script>

</body>
</html>