@extends('admin.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Notices
                <small>Generate notices reports</small>
            </h2>
        </div>
        <div class="card-body card-padding">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="box-title m-t-30">Select Report to generate</h5>
                    <form method="POST" action="{{route('admin.reports.notices.generate')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <select class="select2 form-control selectpicker" name="report">
                                        <option value="" selected >---------------Select--------------</option>
                                        <option value="all">All Notices</option>
                                        <option value="approved">Approved</option>
                                        <option value="pending">Pending</option>
                                        <option value="rejected">Rejected</option>

                                    </select>
                                </div>
                            </div>
                        </div>

                <div class="col-md-12">
                    <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Generate
                    </button>
                </div>
                </form>
                </div>
        </div>
    </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 1, 0 ]
                }, {
                    targets: [ 3],
                    orderData: [ 3, 0 ]
                } ]
            } );
        } );
        function deleteNotice(id){
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Notice!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(isConfirm){

                if (isConfirm) {
                    document.getElementById(id).submit();

                }
            });
        }
        $(document).ready(function() {
            $('#summernote').summernote({
                height:200,
            });
            $('#summernote1').summernote({
                height:200,
            });

        });
        function getData(id){
            var submiturl = "{{URL::to('admin/notices/fetch')}}";
            var content="";
            var file="";
            var description="";
            $.ajax({
                url:submiturl+ '/'+id,
                type: 'GET',
                data: '',
                success: function(data){
                    console.log(data);
                    if(data.description ==null){

                        $('#div').html('<label for="content">Content</label><textarea rows="8" cols="34" class="form-control" id="summernote" name="content"></textarea>');
//                        $('#summernote').summernote({
//                            height:200,
//                        });
                    }
                    if(data.content ==null){
                        $('#div').html(' ');
                        file+='<label for="description">Decription</label><textarea rows="8" cols="34" class="form-control html-editor" id="fileUpload" name="description"></textarea>';
                        $('#div').append(file);
//                        $('#fileUpload').summernote({
//                            height:200,
//                        });

                    }
                    $("input[name='title']").val(data.title);
                    $("textarea[name='description']").summernote('code',data.description);
                    $("textarea[name='content']").summernote('code',data.content);
//                    $("textarea[name='description']").html(data.description);
//                    $("textarea[name='content']").val(data.content);
                },
                error: function (xhr) {
                    console.log("xhr=" + xhr);
                }
            });


            $("input[name='id']").val(id);
            $("#editnotice").modal("show");
        }

        function update()
        {
            var form = $("#updateform");

        }


    </script>
@endsection