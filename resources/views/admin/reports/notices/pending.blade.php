<!DOCTYPE html>
<html>
<head>
    <title>Notice Reports</title>

    <link href="{{ asset('bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    {{--<link rel="stylesheet" href="{{asset('dataTables.bootstrap.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('public/plugins/dataTables/responsive.bootstrap.min.css')}}">
    <style type="text/css">
        hr.style2 {
            border-top: 3px double #8c8b8b !important;
            height: 1px;
        }
    </style>
</head>
<body>

<div style="text-align: center"><img style="text-align: center" width="125" height="125" src="{{URL::to('logo.png')}}" class="img-thumbnail"></div>
<figcaption><h3 style="text-align: center">Online NoticeBoard System</h3></figcaption>
<hr class="style2" style="font-size: larger;">
<h2 style="text-align: center">Pending Notices</h2>
<p style="text-align: right">
    <?php
    date_default_timezone_set('Africa/Nairobi');
    echo " <b style='text-align:right!important;'> Printed On</b> : " . date("Y/m/d") . "<b> At</b> " . date("h:i:sa");
    ?>
</p>
@foreach($notices as $key=>$notice)
    @if($notice->approved==0)
        <div>
            <div><h3>{{$notice->title}}</h3></div>
            <p><b>Created by:</b> <span>{{$notice->user->name}}</span></p>
            @if($notice->user->roles->name=="admin")
                <p><b>Role:</b> <span>System Administrator</span></p>
            @else
                <p><b>Number</b> <span>{{$notice->user->admission_staff_no}}</span></p>
                <p><b>Faculty:</b> <span>{{$notice->user->faculty->faculty}}</span></p>
            @endif
            <p><b>Created on:</b> <span>{{Carbon\Carbon::parse($notice->created_at)->toDayDateTimeString()}} </span></p>

        </div>
        <div class="justify-content-center">
            @if($notice->type=="text_notice")
                {!! $notice->content !!}
            @elseif($notice->type=="file_upload")
                @if($notice->mime=="image/png"||$notice->mime=="image/jpg" ||$notice->mime=="image/jpeg")
                    <div style="text-align: center"><img style="text-align: center"  src="{{URL::to('notices/'.$notice->file)}}" class="img-thumbnail"></div>
                @endif
                {!! $notice->description !!}
            @endif
        </div>
        <hr class="style2" style="font-size: larger;">
    @endif
@endforeach
<!-- jQuery -->
<script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('bootstrap/dist/js/bootstrap.min.js') }}"></script>

</body>
</html>