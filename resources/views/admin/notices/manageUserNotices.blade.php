@extends('admin.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Manage User Notices</h2>
            <small>Created by both students and lecturers</small>
        </div>
        <div class="card-body card-padding">
            <div class="form-wizard-basic fw-container">
                <ul class="tab-nav" role="tablist">
                    <li  class="active" ><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">All Notices</a></li>
                    <li><a href="#tab2" data-toggle="tab">Approved Notices</a></li>
                    <li><a href="#tab3" data-toggle="tab">Pending Notices</a></li>
                    <li><a href="#tab4" data-toggle="tab">Denied Approval</a></li>

                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="tab1">
                        <br>
                        <table id="table1" class="table table-striped table-vmiddle">
                            <thead>
                            <tr>
                                <th>Name </th>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Created</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($notices as $notice)

                                <tr>
                                    <td>{{$notice->user->name}}</td>
                                    <td>{{$notice->title}}</td>
                                    @if($notice->content !='')
                                        <td class="center">{!!$notice->TableContent!!}</td>
                                    @elseif($notice->description !='')
                                        <td class="center">{!!$notice->DescriptionContent!!}</td>
                                    @endif

                                    <td class="center"> {{$notice->created_at->toDayDateTimeString()}} </td>
                                    @if($notice->approved==0)
                                        <td class="center">
                                            <a class="btn btn-warning btn-xs" href="#">
                                                <i class="glyphicon glyphicon-remove icon-red"></i>
                                                Pending
                                            </a>
                                        </td>
                                    @elseif($notice->approved==1)
                                        <td class="center">
                                            <a class="btn btn-success btn-xs" href="#">
                                                <i class="glyphicon glyphicon-0k icon-red"></i>
                                                Approved
                                            </a>
                                        </td>
                                    @else
                                        <td class="center">
                                            <a class="btn btn-danger btn-xs" href="#">
                                                <i class="glyphicon glyphicon-ban-circle icon-red"></i>
                                                Denied
                                            </a>
                                        </td>
                                    @endif
                                </tr>

                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <div class="tab-pane fade" id="tab2">
                        <br>
                        <table id="table2" class="table table-striped table-vmiddle">
                            <thead>
                            <tr>
                                <th> Name </th>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Created</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($notices as $notice)
                                @if($notice->approved==1)
                                <tr>
                                    <td>{{$notice->user->name}}</td>
                                    <td>{{$notice->title}}</td>
                                    @if($notice->content !='')
                                        <td class="center">{!!$notice->TableContent!!}</td>
                                    @elseif($notice->description !='')
                                        <td class="center">{!!$notice->DescriptionContent!!}</td>
                                    @endif

                                    <td class="center"> {{$notice->created_at->toDayDateTimeString()}} </td>
                                    @if($notice->approved==0)
                                        <td class="center">
                                            <a class="btn btn-warning btn-xs" href="#">
                                                <i class="glyphicon glyphicon-remove icon-red"></i>
                                                Pending
                                            </a>
                                        </td>
                                    @elseif($notice->approved==1)
                                        <td class="center">
                                            <a class="btn btn-success btn-xs" href="#">
                                                <i class="glyphicon glyphicon-0k icon-red"></i>
                                                Approved
                                            </a>
                                        </td>
                                    @else
                                        <td class="center">
                                            <a class="btn btn-danger btn-xs" href="#">
                                                <i class="glyphicon glyphicon-ban-circle icon-red"></i>
                                                Denied
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                                @endif
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <div class="tab-pane fade" id="tab3">
                        <br>
                        <table id="table3" class="table table-striped table-vmiddle">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name </th>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Created</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($notices as $key=>$notice)
                                @if($notice->approved==0)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$notice->user->name}}</td>
                                        <td>{{$notice->title}}</td>
                                        @if($notice->content !='')
                                            <td class="center">{!!$notice->TableContent!!}</td>
                                        @elseif($notice->description !='')
                                            <td class="center">{!!$notice->DescriptionContent!!}</td>
                                        @endif

                                        <td class="center"> {{$notice->created_at->toDayDateTimeString()}} </td>
                                        <td class="center">
                                            <a class="btn btn-success btn-xs" href="#" onclick="return viewToApprove('{{$notice->id}}')">
                                                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                                                View
                                            </a>
                                        </td>

                                    </tr>
                                @endif
                            @empty
                                <tr><td colspan="5" style="text-align: center; color: #03A9F4;">No New Clients at the moment</td></tr>
                                empty
                            @endforelse
                            </tbody>

                        </table>
                    </div>
                    <div class="tab-pane "  id="tab4">
                        <br>
                        <table id="table4" class="table table-striped table-vmiddle">
                            <thead>
                            <tr>
                                <th>Name </th>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Created</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($notices as $notice)
                                @if($notice->approved==2)

                                <tr>
                                    <td>{{$notice->user->name}}</td>
                                    <td>{{$notice->title}}</td>
                                    @if($notice->content !='')
                                        <td class="center">{!!$notice->TableContent!!}</td>
                                    @elseif($notice->description !='')
                                        <td class="center">{!!$notice->DescriptionContent!!}</td>
                                    @endif

                                    <td class="center"> {{$notice->created_at->toDayDateTimeString()}} </td>
                                    @if($notice->approved==0)
                                        <td class="center">
                                            <a class="btn btn-warning btn-xs" href="#">
                                                <i class="glyphicon glyphicon-remove icon-red"></i>
                                                Pending
                                            </a>
                                        </td>
                                    @elseif($notice->approved==1)
                                        <td class="center">
                                            <a class="btn btn-success btn-xs" href="#">
                                                <i class="glyphicon glyphicon-0k icon-red"></i>
                                                Approved
                                            </a>
                                        </td>
                                    @else
                                        <td class="center">
                                            <a class="btn btn-danger btn-xs" href="#">
                                                <i class="glyphicon glyphicon-ban-circle icon-red"></i>
                                                Denied
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                            @endforeach
                            </tbody>

                        </table>
                    </div>

                    <ul class="fw-footer pagination wizard">
                        <li class="previous first"><a class="a-prevent" href=""><i
                                        class="zmdi zmdi-more-horiz"></i></a></li>
                        <li class="previous"><a class="a-prevent" href=""><i
                                        class="zmdi zmdi-chevron-left"></i></a></li>
                        <li class="next"><a class="a-prevent" href=""><i
                                        class="zmdi zmdi-chevron-right"></i></a></li>
                        <li class="next last"><a class="a-prevent" href=""><i
                                        class="zmdi zmdi-more-horiz"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="approveNotice" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">Approve Notice</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form id="updateform" method="POST" action="{{route('userNotices.approve')}}">

                        {{csrf_field()}}
                        <input type="hidden" name="id">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" disabled="disabled">
                        </div>

                        <div class="form-group" id="div">

                        </div>
                        <div class="form-group">
                            <label for="approved">Action</label>
                            <select class="form-control type" name="approved">
                                <option disabled="disabled" selected="selected">--execute--</option>
                                <option value="approved">Approve</option>
                                <option value="deny">Deny</option>
                            </select>
                        </div>
                        <div class="form-group" id="reason">

                        </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                        <div class="btn-group" role="group">
                            <button type="submit"   class="btn btn-primary btn-hover-green" data-action="save" role="button">Execute</button>
                        </div>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- content ends -->
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(window).on("load", function () {
            $('#file').val('');
        })

        $(document).ready(function() {
            $('#table1').DataTable( {
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 1, 0 ]
                }, {
                    targets: [ 3],
                    orderData: [ 3, 0 ]
                } ]
            } );
            $('#table2').DataTable( {
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 1, 0 ]
                }, {
                    targets: [ 3],
                    orderData: [ 3, 0 ]
                } ]
            } );
            $('#table3').DataTable( {
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 1, 0 ]
                }, {
                    targets: [ 3],
                    orderData: [ 3, 0 ]
                } ]
            } );
            $('#table4').DataTable( {
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 1, 0 ]
                }, {
                    targets: [ 3],
                    orderData: [ 3, 0 ]
                } ]
            } );
        } );
        function viewToApprove(id){
            var editurl="{{ URL::to('admin/notice')}}";
            var file="";
            $.ajax({
                url:editurl+'/'+id,
                type:'GET',
                data:'',
                success: function(data){
                    if(data.description ==null){
                        $('#div').html('<label for="content">Content</label><textarea class="form-control" id="summernote" name="content"></textarea>');
                        $('#summernote').summernote({
                            height:200,
                        });
                    }
                    if(data.content ==null){
                        $('#div').html(' ');
                        file+='<label for="description">Decription</label><textarea class="form-control" id="summernote1" name="description"></textarea>';
                        $('#div').append(file);
                    }
                    $("input[name='title']").val(data.title);
                    $("textarea[name='description']").summernote('code',data.description);
                    $('#summernote').summernote('disable');
                    $('#summernote1').summernote('disable');
                    $("textarea[name='content']").summernote('code',data.content);
                },
                error:function(xhr){
                    console.log("xhr=" + xhr);
                }
            });
            $("input[name='id'").val(id);
            $("#approveNotice").modal("show");
        }
        $(document).on('change','.type', function(){
            var type=$(this).val();
            console.log(type);
            var div=$(this).parent().parent();
            var file2=" ";
            if(type=='deny'){
                file2+='<label for="reason">Reason</label><textarea class="form-control" name="reason" required="required"></textarea>';
                div.find('#reason').html(file2);
            }
            if(type=='approved'){
                div.find('#reason').html(' ');
            }
        });

    </script>

@endsection