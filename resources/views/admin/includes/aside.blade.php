            <aside id="sidebar" class="sidebar c-overflow">
                <div class="s-profile">
                    <a href="" data-ma-action="profile-menu-toggle">
                        <div class="sp-pic">
                            <img src="{{url('img/profile-pics/1.jpg')}}" alt="">
                        </div>

                        <div class="sp-info">
                            @if(Auth::check())
                            {{Auth::user()->name}}
                            @else
                                header({{URL::to('logout')}})
                            @endif
                            <i class="zmdi zmdi-caret-down"></i>
                        </div>
                    </a>
                    <ul class="main-menu">                        
                        <li>
                            <a href="{{route('admin.password')}}"><i class="zmdi zmdi-input-antenna"></i> Change Password</a>

                        </li>                        
                        <li>
                            <a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{csrf_field()}}
                            </form>
                        </li>
                    </ul>
                </div>

                <ul class="main-menu">
                    <li class="active">
                        <a href="{{route('admin.index')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>

                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-accounts"></i> Users</a>

                        <ul>
                            <li><a href="{{route('users.create')}}">Create</a></li>
                            <li><a href="{{route('users.manage')}}">Manage</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-notifications-active"></i> Notices</a>

                        <ul>
                            <li><a href="{{route('notices.index')}}">Create</a></li>
                            <li><a href="{{route('notices.manage')}}" >Manage</a> </li>
                            <li><a href="{{route('usernotices.manage')}}">User Notices</a></li>
                        </ul>
                    </li>  
                    
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-calendar-note"></i> Events</a>
     
                        <ul>
                            <li><a href="{{route('events.create')}}">Create</a></li>
                            <li><a href="{{route('events.manage')}}">Manage</a></li>
                            <li><a href="{{route('systemEvents.manage')}}">User Events</a></li>
                        </ul>
                        
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-widgets"></i> Time Tables</a>

                        <ul>
                            <li><a href="{{route('timetables.create')}}">Create</a></li>
                            <li><a href="{{route('timetables.manage')}}">Manage</a></li>
                        </ul>

                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-layers"></i> Reports</a>

                        <ul>

                            <li><a href="{{route('admin.reports.students')}}">Students</a></li>
                            <li><a href="{{route('admin.reports.supervisors')}}">Lecturers</a></li>
                            <li><a href="{{route('admin.reports.notices')}}">Notices</a></li>
                            <li><a href="{{route('admin.reports.events')}}">Events</a></li>

                        </ul>

                    </li>

                </ul>
            </aside>